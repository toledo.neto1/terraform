# AWS compute instance
resource "aws_instance" "app_server" {
  name         = "terraform-node"
  ami           = "ami-830c94e3"
  instance_type = "t2.micro"
  availability_zone = "us-east-1b"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}

