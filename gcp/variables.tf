#### GCP Vars
variable "CREDENTIALS_FILE" {
  description = "GCP Credentials"
}

variable "PROJECT_ID" {
  description = "project id"
}

variable "REGION" {
  description = "GCP Region"
}

variable "ZONE" {
  description = "GCP Zone"
}
