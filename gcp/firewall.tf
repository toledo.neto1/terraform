# bastion allow ssh firewall
resource "google_compute_firewall" "terraform-ssh" {
  name    = "terraform-node-ssh"
  description = "Allow comm at port 22 on bastion"
  network = "default"
  project = var.PROJECT_ID

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["0.0.0.0/0"]
  direction = "INGRESS"
  priority = 900
  target_tags = ["terraform-node"]
}

resource "google_compute_firewall" "update-firewall" {
  name    = "update-firewall"
  description = "Update this firewall rule"
  network = "default"
  project = var.PROJECT_ID

  allow {
    protocol = "tcp"
    ports    = ["5000"]
  }

  source_ranges = ["0.0.0.0/0"]
  direction = "INGRESS"
  priority = 900
  target_tags = ["terraform-node"]
}

resource "google_compute_firewall" "delete-firewall" {
  name    = "delete-firewall"
  description = "Delete this firewall rule"
  network = "default"
  project = var.PROJECT_ID

  allow {
    protocol = "tcp"
    ports    = ["9090"]
  }

  source_ranges = ["0.0.0.0/0"]
  direction = "INGRESS"
  priority = 900
  target_tags = ["terraform-node"]
}
