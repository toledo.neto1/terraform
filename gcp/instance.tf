# GCP compute instance
resource "google_compute_instance" "terraform-gcp" {
  name         = "terraform-node"
  machine_type = "g1-small"
  zone         = "us-central1-c"
  project      = "terraform-306317"
  tags         = ["terraform-node"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10" # more here https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_image
    }
  }

  network_interface {
    network = "default"
    subnetwork = "default"

  }
}
