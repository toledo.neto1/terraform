# Terraform

## Módulos terraform usados

### GCP
* Google provider: [link](https://registry.terraform.io/providers/hashicorp/google/latest/docs)
* Compute instance: [link](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_instance)
* Compute Firewall: [link](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall)

### AWS
* EC2 instance: [link](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)
